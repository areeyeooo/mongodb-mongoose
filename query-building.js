const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')
const Room = require('./models/Room')
const Building = require('./models/Building')

async function main () {
    const room = await Room.findOne({ capacity: { $gte: 100 } }).populate('building')
    console.log(room)
    console.log('-----------------------')
    const rooms = await Room.find({ capacity: { $gte: 100 } }).populate('building')
    console.log(rooms)
}

main().then( function () {
    console.log('Finish')
})

